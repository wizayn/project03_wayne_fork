﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Engine))]
public class EngineEditor : Editor
{
    Engine engineScript;

    void Awake()
    {
        engineScript = (Engine)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        if(GUILayout.Button("Open Editor"))
        {
            EngineWindow.Init();
        }

        serializedObject.ApplyModifiedProperties();
    }

    void PrintInformation()
    {
        Debug.Log("Printing movement Engine Information!");
        Debug.Log("Movement Length: " + engineScript.movementList.Count);

        foreach(ScriptMovement moveScript in engineScript.movementList)
        {
            Debug.Log("\tMovement printing...");
            Debug.Log("\t" + moveScript.type.ToString() + ".");
            Debug.Log("\tEnd Point Name: " + moveScript.endPoint.gameObject.name + ".");
        }
    }
}
