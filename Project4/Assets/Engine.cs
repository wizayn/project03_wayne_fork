﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Engine : MonoBehaviour
{
    public GameObject playerObj;
    public GameObject myHead;

    public Image splatterImage;
    public Image blackImage;

    public List<ScriptsEffects> effectsList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;
    
    int currentWaypoint;

    public int lastEditedMovement = 0, lastEditedFacing = 0;

    void Start()
    {
        playerObj = GameObject.FindWithTag("Player");
        myHead = GameObject.Find("myHead");
        StartCoroutine("MovementEngine");
        StartCoroutine("FacingEngine");
        StartCoroutine("EffectsEngine");
        Logger.LogA("Hello!", true);

    }

    IEnumerator MovementEngine()
    {
        for (int i = 0; i < movementList.Count; i++)
        {
            switch(movementList[i].type)
            {
                case MovementTypes.STRAIGHT:
                    Debug.Log("Doing straight line movement");
                    yield return StartCoroutine(StraightLine(movementList[i].speed, movementList[i].endPoint.transform.position));
                    break;
                case MovementTypes.WAIT:
                    Debug.Log("Doing wait");
                    yield return StartCoroutine("WaitMovement", i);
                    break;
                case MovementTypes.BEIZIER:
                    Debug.Log("Doing Beizier");
                    yield return StartCoroutine("BezierCurve", i);
                    break;
                case MovementTypes.ROTATEANDRETURN:
                    yield return StartCoroutine("RotateAndReturn", i);
                    break;

            }
        }
    }

    IEnumerator FacingEngine()
    {
        for(int i = 0; i < facingList.Count; i++)
        {
            switch (facingList[i].type)
            {
                case FacingTypes.LOOKANDRETURN:
                    yield return StartCoroutine(LookAtTarget(i, facingList[i].speed, facingList[i].lookTarget.transform.position));
                    break;
                case FacingTypes.FREECAMERA:
                    yield return StartCoroutine("FreeLook", i);
                    break;
                case FacingTypes.WAIT:
                    yield return StartCoroutine("WaitFacing", i);
                    break;
                case FacingTypes.FORCEDLOOK:
                    yield return StartCoroutine((ForcedLook(i, facingList[i].speed, facingList[i].lookTarget.transform.position)));
                    break;
            }
        }
        yield return null;
    }
    
    IEnumerator EffectsEngine()
    {
        for(int i = 0; i < effectsList.Count; i++)
        {
            switch (effectsList[i].type)
            {
                case EffectTypes.SHAKE:
                    yield return StartCoroutine("CameraShake", i);
                    break;
                case EffectTypes.SPLATTER:
                    yield return StartCoroutine("Splatter", i);
                    break;
                case EffectTypes.FADE:
                    yield return StartCoroutine("Fade", i);
                    break;
                case EffectTypes.WAIT:
                    yield return StartCoroutine("WaitEffects", i);
                    break;
            }
        }
        yield return null;
    }

    //*************************
    // Movement Coroutines
    //*************************
    IEnumerator StraightLine(float time, Vector3 endPos)
    {
        
        float elapsedTime = 0f;
        Vector3 startingPos = playerObj.transform.position;
        while(elapsedTime < time)
        {
            playerObj.transform.position = Vector3.Lerp(startingPos, endPos, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        playerObj.transform.position = endPos;
    }

    IEnumerator BezierCurve(int waypointIndex)
    {

        // Get current player position and rotation
        Vector3 beginningPosition = movementList[waypointIndex].startPoint.transform.position; //playerObj.transform.position;

        Vector3 endingPosition = movementList[waypointIndex].endPoint.transform.position;
        Vector3 curvePosition = movementList[waypointIndex].beizeirCurvePoint.transform.position;

        float elapsedTime = 0f;
        Vector3 startingPos = playerObj.transform.position;
        while (elapsedTime < movementList[waypointIndex].speed)
        {
            playerObj.transform.position = GetPoint(beginningPosition, endingPosition, curvePosition, (elapsedTime / movementList[waypointIndex].speed));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        playerObj.transform.position = endingPosition;
    }

    IEnumerator RotateAndReturn(int waypointIndex)
    {
        GameObject camera = GameObject.Find("myHead");
        Vector3 lookTarget = facingList[waypointIndex].lookTarget.transform.position;
        Quaternion startRotation = camera.transform.rotation;
        Quaternion targetRotation = Quaternion.LookRotation(lookTarget);
        Quaternion lookRotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * facingList[waypointIndex].speed);


        yield return new WaitForEndOfFrame();
    }

    IEnumerator WaitMovement(int waypointIndex)
    {
        yield return new WaitForSeconds(movementList[waypointIndex].waitTime);
    }

    /// <summary> GetPoint(args)
    /// Gets a point along a Bezier Curve
    /// </summary>
    /// <param name="start">Start point on the curve.</param>
    /// <param name="end">End point on the curve.</param>
    /// <param name="curve">Handle for the curve.</param>
    /// <param name="t">Steps along the curve.</param>
    /// <returns>The point we are after.</returns>
    public Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1.0f - t;
        return oneMinusT * oneMinusT * start + 2.0f * oneMinusT * t * curve + t * t * end;

    }

    //******************************
    // Camera Facings Coroutines
    //******************************
    IEnumerator LookAtTarget(int i, float time, Vector3 lookPosition)
    {
        GameObject.Find("myHead").GetComponentInChildren<MouseLook>().enabled = false;
        float elapsedTime = 0f;
        Quaternion startRotation = playerObj.transform.rotation;
        Quaternion targetRotation = Quaternion.LookRotation((lookPosition - playerObj.transform.position).normalized);
        
        //Look to the target
        while (elapsedTime < time)
        {
            myHead.transform.rotation = Quaternion.Lerp(startRotation, targetRotation, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //Look back to original position
        elapsedTime = 0;
        while (elapsedTime < time)
        {
            myHead.transform.rotation = Quaternion.Lerp(targetRotation, Quaternion.Euler(playerObj.transform.forward), (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        playerObj.transform.rotation = Quaternion.Euler(playerObj.transform.forward);
        facingList[i].canlook = true;
        GameObject.Find("myHead").GetComponentInChildren<MouseLook>().enabled = true;
    }

    IEnumerator FreeLook(int waypointIndex)
    {        
        if(facingList[waypointIndex].canlook == true)
        {
            GameObject.Find("myHead").GetComponentInChildren<MouseLook>().enabled = true;
            //Debug.Log("Free look should be true" + (bool)facingList[waypointIndex].canlook);
        }
        else
        {
            GameObject.Find("myHead").GetComponentInChildren<MouseLook>().enabled = false;
            //Debug.Log("Free look should be false" + (bool)facingList[waypointIndex].canlook);
        }
        yield return null;
    }

    IEnumerator ForcedLook(int waypointIndex, float time, Vector3 lookPosition)
    {
		myHead.GetComponentInChildren<MouseLook>().enabled = false;
		Debug.Log ("Mouse Look enabled?" + myHead.GetComponentInChildren<MouseLook> ().enabled.ToString ());
        float elapsedTime = 0f;
        Quaternion startRotation = playerObj.transform.rotation;
        Quaternion targetRotation = Quaternion.LookRotation((lookPosition - playerObj.transform.position).normalized);
        //Disable free look
        
        //Get location to look at
        while (elapsedTime < time)
        {
            playerObj.transform.rotation = Quaternion.Lerp(startRotation, targetRotation, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        //look at target for duration
        
        //Enable free look
		myHead.GetComponentInChildren<MouseLook>().enabled = true;

        yield return new WaitForEndOfFrame();
    }
    IEnumerator WaitFacing(int waypointIndex)
    {
        yield return new WaitForSeconds(facingList[waypointIndex].waitTime);
    }

    IEnumerator TimeToForceLook(int waypointIndex)
    {
        yield return new WaitForSeconds(facingList[waypointIndex].timeToLook);
    }

    //*****************************
    // Effects Coroutines
    //*****************************
    IEnumerator CameraShake(int waypointIndex)
    {
        myHead.GetComponentInChildren<MouseLook>().enabled = false;
        float startTime = Time.time;
        float shakingTime = effectsList[waypointIndex].duration;
        float intensity = effectsList[waypointIndex].intensity;

        while (Time.time < startTime + shakingTime)
        {
            myHead.GetComponent<MeshRenderer>().enabled = false;
            myHead.transform.localPosition = Random.insideUnitSphere * intensity;
            yield return new WaitForEndOfFrame();
        }
        myHead.transform.localPosition = Vector3.zero;
        myHead.GetComponent<MeshRenderer>().enabled = true;
        myHead.GetComponentInChildren<MouseLook>().enabled = true;
    }

    IEnumerator Splatter(int waypointIndex)
    {
        float startTime = Time.time;
        float splatterDurationTime = effectsList[waypointIndex].duration;
        splatterImage.enabled = true;

        bool useFadeIn = effectsList[waypointIndex].useSplatterFadeIn;
        bool useFadeOut = effectsList[waypointIndex].useSplatterFadeOut;
        float fadeInTime = effectsList[waypointIndex].splatterFadeInTime;
        float fadeOutTime = effectsList[waypointIndex].splatterFadeOutTime;


        if (useFadeIn == true)
        {

            Color endColor = splatterImage.color;
            endColor.a = 1f;

            float elapsedTime = 0f;

            Color startColor = splatterImage.color;
            startColor.a = 0.0f;

            while (elapsedTime < fadeInTime)
            {
                Color tempColor = splatterImage.color;

                tempColor.a = Mathf.Lerp(startColor.a, endColor.a, (elapsedTime / fadeInTime));
                splatterImage.color = tempColor;

                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Color newTempColor = splatterImage.color;
            newTempColor.a = 1;
            splatterImage.color = newTempColor;
        }

        //Turn on splatter image
        while (Time.time < startTime + splatterDurationTime)
        {
            splatterImage.enabled = true;
            yield return new WaitForEndOfFrame();
        }


        if (useFadeOut == true)
        {
            Color endColor = splatterImage.color;
            endColor.a = 0f;

            float elapsedTime = 0f;

            Color startColor = splatterImage.color;
            startColor.a = 1.0f;

            while (elapsedTime < fadeOutTime)
            {
                Color tempColor = splatterImage.color;

                tempColor.a = Mathf.Lerp(startColor.a, endColor.a, (elapsedTime / fadeOutTime));
                splatterImage.color = tempColor;

                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Color newTempColor = splatterImage.color;
            newTempColor.a = 0f;
            splatterImage.color = newTempColor;
        }

        splatterImage.enabled = false;
    }

    IEnumerator Fade(int waypointIndex)
    {
        //float startTime = Time.time;
        blackImage.enabled = true;

        bool fadeInBlack = effectsList[waypointIndex].fadeInFromBlack;
        bool fadeOutBlack = effectsList[waypointIndex].fadeOutToBlack;
        float fadeInBlackTime = effectsList[waypointIndex].fadeInTime;
        float fadeOutBlackTime = effectsList[waypointIndex].fadeOutTime;

        if (fadeInBlack == true)
        {
            Color endColor = blackImage.color;
            endColor.a = 0f;

            float elapsedTime = 0f;

            Color startColor = blackImage.color;
            startColor.a = 1.0f;

            while (elapsedTime < fadeOutBlackTime)
            {
                Color tempColor = blackImage.color;

                tempColor.a = Mathf.Lerp(startColor.a, endColor.a, (elapsedTime / fadeOutBlackTime));
                blackImage.color = tempColor;

                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Color newTempColor = blackImage.color;
            newTempColor.a = 0f;
            blackImage.color = newTempColor;
        }


        if (fadeOutBlack == true)
        {
            Color endColor = blackImage.color;
            endColor.a = 1f;

            float elapsedTime = 0f;

            Color startColor = blackImage.color;
            startColor.a = 0.0f;

            while (elapsedTime < fadeInBlackTime)
            {
                Color tempColor = blackImage.color;

                tempColor.a = Mathf.Lerp(startColor.a, endColor.a, (elapsedTime / fadeInBlackTime));
                blackImage.color = tempColor;

                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            Color newTempColor = blackImage.color;
            newTempColor.a = 1;
            blackImage.color = newTempColor;
        }

        blackImage.enabled = false;
    }

    IEnumerator WaitEffects(int waypointIndex)
    {
        yield return new WaitForSeconds(effectsList[waypointIndex].waitTime);
    }  
}
