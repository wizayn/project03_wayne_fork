﻿using UnityEngine;
using System.Collections;

public enum WaypointTypes
{
    MOVEMENT,
    FACING,
    EFFECT
}

public class Engine : MonoBehaviour
{

    public Waypoint[] waypoints;

    IEnumerator start()
    {
        //Loop through the waypoints array
        for (int i = 0; i < waypoints.Length; i++)
        {

            //Determine waypoint type we are looking at
            switch (waypoints[i].waypointType)
            {
                //If we are a movement waypoint
                case WaypointTypes.MOVEMENT:
                    yield return StartCoroutine(MovementEngine(i));
                    break;
                case WaypointTypes.FACING:
                    break;
                case WaypointTypes.EFFECT:
                    break;
                default:
                    break;
            }
        }
    }

    IEnumerator MovementEngine(int waypointNum)
    {

        switch (waypoints[waypointNum].moveType)
        {
            case MovementType.BEIZIER_CURVE:
                float bezElapsedTime = 0f;
                float bezmoveTime = waypoints[waypointNum].bezierMoveTime;


                float bezMoveTime = waypoints[waypointNum].bezierMoveTime;
                while (bezElapsedTime < bezMoveTime)
                {

                }
                break;

            case MovementType.LOOK_CHAIN:
                break;

            case MovementType.STRAIGHT_LINE:
                Debug.Log("Test");
                Transform endPoint = waypoints[waypointNum].endPoint;
                float moveTime = waypoints[waypointNum].timeToGetThere;

                //Lerp along the line
                float elapsedTime = 0f;
                Vector3 startPoint = transform.position;

                while (elapsedTime < moveTime)
                {
                    Debug.Log(elapsedTime);
                    transform.position = Vector3.Lerp(startPoint, endPoint.position, (elapsedTime / moveTime));
                    elapsedTime += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                break;
            case MovementType.WAIT:
                yield return new WaitForSeconds(waypoints[waypointNum].waitTime);
                break;
            default:
                break;
        }

        yield return null;
    }

     Vector3 BezierCurve(Vector3 start, Vector3 control, Vector3 end, float t)
    {

        return (((1 - t) * (1 - t)) * start) + (2 * t * (1 - t) * control) + ((t * t) * end);
    }
}
