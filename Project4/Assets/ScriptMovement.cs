﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptMovement
{
    public MovementTypes type;

    public GameObject startPoint;
    public GameObject endPoint;
    public GameObject beizeirCurvePoint;

    public float speed;
    public float waitTime;
    public float moveTime;

    public string name;
}
