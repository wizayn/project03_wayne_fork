﻿using UnityEngine;
using System.Collections;
using UnityEditor;

//MovementDrawer
[CustomPropertyDrawer (typeof (WaypointMovement))]
public class MovementDrawer : PropertyDrawer
{
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty (position, label, property);

		position = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), label);

		var indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 5;

		var moveType = new Rect (position.x, position.y, 30, position.height);
		var unitRect = new Rect (position.x+35, position.y, 50, position.height);
		var nameRect = new Rect (position.x+90, position.y, position.width-90, position.height);

		EditorGUI.PropertyField (moveType, property.FindPropertyRelative ("moveType"));


		/**
		EditorGUI.PropertyField (amountRect, property.FindPropertyRelative ("amount"), GUIContent.none);
		EditorGUI.PropertyField (unitRect, property.FindPropertyRelative ("unit"), GUIContent.none);
		EditorGUI.PropertyField (nameRect, property.FindPropertyRelative ("name"), GUIContent.none);
		  */

		EditorGUI.indentLevel = indent;

		EditorGUI.EndProperty ();
	}

}
