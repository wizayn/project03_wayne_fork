﻿using UnityEngine;
using System.Collections;

public enum MovementType
{
	STRAIGHT_LINE,
	BEIZIER_CURVE,
	WAIT,
	LOOK_CHAIN
}

public enum FacingType
{

}

public enum EffectType
{

}

[System.Serializable]
public class Waypoint 
{
	//Store all the waypoints the designer has created
	public WaypointTypes waypointType;

	/*************************
	 		MOVEMENT
	*************************/
	public MovementType moveType;

	//STRAIGHT LINE
	public Transform endPoint;
	public float timeToGetThere;

	//WAIT
	public float waitTime;

	//BEIZURE
	public float bezierMoveTime;
	public Transform bezierEndPoint;
	public Transform bezierCurvePoint;

	//ROTATION CHAIN
	public Transform[] rotatePoints;
	public float[] rotateSpeed;

	/********************
	 		Facing
	********************/


	//Store all the facing types the designer has created
	public FacingType faceType;

	//Free movement

	//Look and Return
	public Transform lookPoint;
	public float timeToLook;


	/********************
	 		EFFECTS
	********************/	

	//Store all the effect types the designer has created
	public EffectType effect;

	//Camera Shake
	public float shakeIntensity;
	public float timeToShake;

	//Splatter
	public float splatterDuration;
	public float splatterFadeTime;

	//Fade
	public float fadeTime;

}
