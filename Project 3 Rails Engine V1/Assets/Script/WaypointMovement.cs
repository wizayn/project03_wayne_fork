﻿using UnityEngine;
using System.Collections;


public enum MovementType
{
	SLM,
	LAR,
	LC,
	Wait
}

public class WaypointMovement 
{
	public GameObject pointToLook;
	public MovementType moveType;

	public float minDist;
	public float speed;
	public float moveTime;
	public float timeOnPoint;
	public float timeToWait;
}

public class movement : MonoBehaviour{

}