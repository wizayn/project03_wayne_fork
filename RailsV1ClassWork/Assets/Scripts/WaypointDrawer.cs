﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(Waypoint))]
public class WaypointDrawer : PropertyDrawer {

	Waypoint thisWaypoint;
	float extraHeight = 100f;

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty (position, label, property);

		//All the editor stuff in here

		//Display the type of waypoint
		//Setup a position to display the variables
		Rect waypointPosition = new Rect(position.x, position.y, position.width, 17f);
		//Get the variable off the script
		SerializedProperty waypointType = property.FindPropertyRelative ("waypointType");
		//Display the variable in the inspector
		EditorGUI.PropertyField (waypointPosition, waypointType);

		//Ask what type of waypoint this is
		switch((WaypointTypes)waypointType.enumValueIndex)
		{
			//Display movement specific information
		case WaypointTypes.MOVEMENT:
				
				//Ask what type of movement this is
			Rect subtypePosition = new Rect (position.x, position.y + 17f, position.width, 17f);
			SerializedProperty movementType = property.FindPropertyRelative ("moveType");
			EditorGUI.PropertyField (subtypePosition, movementType);
			//Display subtype specific information
			switch ((MovementType)movementType.enumValueIndex) 
			{
			case MovementType.BEIZIER_CURVE:

					//2 lines down
				Rect bMoveTimePosition = new Rect (position.x, position.y + (17f *2f), position.width, 17f);

				//set up percentages --
				//first 10% of the width
				//position.width * 0.1 = 10% of the width of the window
				//position.width * 0.4 = 40% of the width of the window
				Rect endPointLabelPosition = new Rect (position.x, position.y + (17f * 3f), position.width * 0.1f, 17f);
				Rect bEndPointPosition = new Rect (position.x + position.width * 0.1f, position.y + (17f * 3f), position.width * 0.4f, 17f);

				Rect curvePointLabelPosition = new Rect (position.x + position.width * 0.5f, position.y + (17f * 3f), position.width * 0.1f, 17f);
				Rect bCurvePointPosition = new Rect (position.x + position.width * 0.6f, position.y + (17f * 3f), position.width * 0.4f, 17f);

				SerializedProperty bMoveTime = property.FindPropertyRelative ("bezierMoveTime");
				SerializedProperty bEndPoint = property.FindPropertyRelative ("bezierEndPoint");
				SerializedProperty bCurvePoint = property.FindPropertyRelative ("bezierCurvePoint");

				EditorGUI.LabelField (endPointLabelPosition, "End Point: ");
				EditorGUI.LabelField (curvePointLabelPosition, "Curve Point: ");

				EditorGUI.PropertyField (bMoveTimePosition, bMoveTime);
				EditorGUI.PropertyField (bEndPointPosition, bEndPoint, GUIContent.none);
				EditorGUI.PropertyField (bCurvePointPosition, bCurvePoint, GUIContent.none);
					break;
			case MovementType.LOOK_CHAIN:
				Rect rotatePointsPosition = new Rect (position.x, position.y + (17f * 2f), position.width, 17f);
				Rect rotatePointsSizePosition = new Rect (position.x, position.y + (17f * 3f), position.width, 17f);

				SerializedProperty rotatePoints = property.FindPropertyRelative ("rotatePoints");

				SerializedProperty rotateSpeeds = property.FindPropertyRelative ("rotateSpeed");

				Rect rotateSpeedElementPosition = new Rect (position.x + position.width / 2, position.y + (17f * 4f), position.width / 2, 17f);
				Rect rotatePointsElementPosition = new Rect (position.x, position.y + (17f * 4f), position.width / 2, 17f);

				//Display the rotate points array
				EditorGUI.PropertyField (rotatePointsPosition, rotatePoints);
				if (rotatePoints.isExpanded) 
				{
					EditorGUI.PropertyField (rotatePointsSizePosition, rotatePoints.FindPropertyRelative ("Array.size"));
					rotateSpeeds.arraySize = rotatePoints.arraySize;

					for (int i = 0; i < rotatePoints.arraySize; i++) 
					{
						EditorGUI.PropertyField (rotatePointsElementPosition, rotatePoints.GetArrayElementAtIndex (i));
						EditorGUI.PropertyField (rotateSpeedElementPosition, rotateSpeeds.GetArrayElementAtIndex (i));

						rotatePointsElementPosition.y += 17f;
						rotateSpeedElementPosition.y += 17f;
					}

					extraHeight = 100 + (17f * rotatePoints.arraySize);
				} 

				//DO THE SPEED ARRAY STUFF
					break;
				case MovementType.STRAIGHT_LINE:
					Rect endPointPosition = new Rect (position.x, position.y + (17f * 2f), position.width, 17f);
					Rect moveTimePosition = new Rect (position.x, position.y + (17f * 3f), position.width, 17f);

					SerializedProperty endPoint = property.FindPropertyRelative ("endPoint");
					SerializedProperty moveTime = property.FindPropertyRelative ("timeToGetThere");

					EditorGUI.PropertyField (endPointPosition, endPoint);
					EditorGUI.PropertyField (moveTimePosition, moveTime);

					break;
				case MovementType.WAIT:
					Rect waitTimePosition = new Rect (position.x, position.y + (17f * 2f), position.width, 17);

					SerializedProperty waitTime = property.FindPropertyRelative ("waitTime");

					EditorGUI.PropertyField (waitTimePosition, waitTime);
					break;
				default:
					break;
			}
			break;
				case WaypointTypes.FACING:
				//Rect subtypePosition = new Rect(position.x, position.y, position.width, 17f);
					break;
				case WaypointTypes.EFFECT:
					break;
				default:
					break;
		}

		EditorGUI.EndProperty ();
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return base.GetPropertyHeight(property, label) + extraHeight;
	}
}
