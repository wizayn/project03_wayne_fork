﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;

public class EngineWindow : EditorWindow
{
    public List<ScriptsEffects> effectsList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;

    Engine engineScript;

    int movementItem = 0;
    int facingItem = 0;
    int effectsItem = 0;

    Vector2 minimumSize = new Vector2(750f, 350f);

    //setup button skins
    GUIStyle miniRight, miniCenter, miniLeft;

    //Position Variables
    float offsetX = 0f, offsetY = 0f, ELEMENT_HEIGHT = 17f;

    public static void Init()
    {
        EngineWindow window = ((EngineWindow)EngineWindow.GetWindow(typeof(EngineWindow)));
        window.Show();
    }

    void OnFocus()
    {
        //reference the script
        engineScript = GameObject.Find("EngineObject").GetComponent<Engine>();

        //BUTTONS
        miniRight = new GUIStyle(EditorStyles.miniButtonRight);
        miniLeft = new GUIStyle(EditorStyles.miniButtonLeft);
        miniCenter = new GUIStyle(EditorStyles.miniButtonMid);

        //Pull info from the script
        movementList = engineScript.movementList;
        effectsList = engineScript.effectsList;
        facingList = engineScript.facingList;

        movementItem = engineScript.lastEditedMovement;
        facingItem = engineScript.lastEditedFacing;

        //Make sure nothing is null
        if (movementList == null)
            movementList = new List<ScriptMovement>();
        if (effectsList == null)
            effectsList = new List<ScriptsEffects>();
        if (facingList == null)
            facingList = new List<ScriptFacings>();

        //Make sure that there is at least one element in the list
        if (movementList.Count <= 0)
            movementList.Add(new ScriptMovement());
        if (effectsList.Count <= 0)
            effectsList.Add(new ScriptsEffects());
        if (facingList.Count <= 0)
            facingList.Add(new ScriptFacings());
    }

    void OnGUI()
    {
        //movementItem = 0;
        //facingItem = 0;
        //effectsItem = 0;

        //built in variable for editor window
        minSize = minimumSize;
        offsetY = 0f;
        MovementGUI();
        EffectsGUI();
        FacingsGUI();
    }

    void MovementGUI()
    {
        offsetX = 0;
        offsetY = 0;
        //sets up rects for showing info
        Rect movementLabel = new Rect(45f, 10f, 250f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        //Setup to draw a line
        Vector2 linePointTop = new Vector2(229, 0);
        Vector2 linePointBottom = new Vector2(229, 500);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.green, 1f, true);

        EditorGUI.LabelField(movementLabel, "MOVEMENT WAYPOINTS");

        Rect numberLabel = new Rect(90f, offsetY, 200f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(numberLabel, ((movementItem + 1) + " / " + movementList.Count));
        offsetY += ELEMENT_HEIGHT + 2f;
        
        //Waypoint Name
        Rect nameLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
        
        EditorGUI.LabelField(nameLabel, "Waypoint Name: ");

        Rect nameInput = new Rect(offsetX + 100f, offsetY, 115f, ELEMENT_HEIGHT);
        offsetY += ELEMENT_HEIGHT +5;
        movementList[movementItem].name = EditorGUI.TextField(nameInput, movementList[movementItem].name);

        Rect typeRect = new Rect(offsetX, offsetY, 220f, ELEMENT_HEIGHT +5);
        offsetY += ELEMENT_HEIGHT + 2f;
        offsetX = 0f;
        movementList[movementItem].type = (MovementTypes)EditorGUI.EnumPopup(typeRect, movementList[movementItem].type);

        //Specific Display Info
        switch(movementList[movementItem].type)
        {
            case MovementTypes.STRAIGHT:
                Rect endLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(endLabel, "End Point: ");

                Rect endRect = new Rect(offsetX, offsetY, 115f, ELEMENT_HEIGHT);
                offsetX = 0f;
                movementList[movementItem].endPoint = (GameObject)EditorGUI.ObjectField(endRect, movementList[movementItem].endPoint, typeof(GameObject), true);

                offsetY += 20f;
                Rect moveSpeedLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(moveSpeedLabel, "Speed: ");
                offsetX += 100;
                Rect speedRect = new Rect(offsetX, offsetY, 30, ELEMENT_HEIGHT);
                movementList[movementItem].speed = EditorGUI.FloatField(speedRect, movementList[movementItem].speed);
                break;
            case MovementTypes.BEIZIER:
                Rect beizierStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 120;
                EditorGUI.LabelField(beizierStartLabel, "End Point: ");

                Rect beizierEndRect = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX = 0f;
                movementList[movementItem].endPoint = (GameObject)EditorGUI.ObjectField(beizierEndRect, movementList[movementItem].endPoint, typeof(GameObject), true);
                

                offsetY += 20f;
                Rect beizierCurveLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 120f;
                EditorGUI.LabelField(beizierCurveLabel, "Curve Point: ");

                Rect beizierCurvePoint = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX = 0f;
                movementList[movementItem].beizeirCurvePoint = (GameObject)EditorGUI.ObjectField(beizierCurvePoint, movementList[movementItem].beizeirCurvePoint, typeof(GameObject), true);

                offsetY += 20;
                Rect beizierSpeedLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100;
                EditorGUI.LabelField(beizierSpeedLabel, "Speed: ");

                offsetX += 20;
                Rect beizierSpeed = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX = 0f;
                movementList[movementItem].speed = EditorGUI.FloatField(beizierSpeed, movementList[movementItem].speed);
                break;
            case MovementTypes.WAIT:
                Rect waitTimeLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(waitTimeLabel, "Wait Time: ");
                
                Rect waitRect = new Rect(offsetX, offsetY, 30, ELEMENT_HEIGHT);
                movementList[movementItem].waitTime = EditorGUI.FloatField(waitRect, movementList[movementItem].waitTime);
                break;

            case MovementTypes.ROTATEANDRETURN:
                Rect lookTargetLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(lookTargetLabel, "Look Target: ");

                offsetX += 125f;
                Rect LookTargetItem = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                facingList[facingItem].lookTarget = (GameObject)EditorGUI.ObjectField(LookTargetItem, facingList[facingItem].lookTarget, typeof (GameObject), true);

                offsetX -= 125f;
                offsetY += 20f;
                Rect speedLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(speedLabel, "Speed to look: ");

                offsetX += 125f;
                Rect speedItem = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                facingList[facingItem].speed = EditorGUI.FloatField(speedItem, facingList[facingItem].speed);

                offsetX -= 125f;
                offsetY += 20f;
                Rect timeToLookLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(timeToLookLabel, "Look Time: ");

                offsetX += 125f;
                Rect timeToLookItem = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                facingList[facingItem].timeToLook = EditorGUI.FloatField(timeToLookItem, facingList[facingItem].timeToLook);
                break;


        }

        //Display Buttons

        //locations for buttons
        Rect prevButtonRect = new Rect(50f, 175f, 40f, ELEMENT_HEIGHT);
        if (movementItem != 0)
        {
            if (GUI.Button(prevButtonRect, "Prev", miniLeft))
            {
                //Debug.Log("Hit previous");
                movementItem--;
            }
        }
        Rect addButtonRect = new Rect(90f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(addButtonRect, "Add", miniCenter))
        {
            Debug.Log("Hit add");
            movementList.Insert(movementItem + 1, new ScriptMovement());
        }
        Rect nextButtonRect = new Rect(130f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(nextButtonRect, "Next", miniRight))
        {
            Debug.Log("Hit next");
            if (movementItem < movementList.Count - 1)
                movementItem++;
            else
            {
                movementList.Add(new ScriptMovement());
                movementItem++;
            }
        }

        if (movementList.Count > 1)
        {
            Rect deleteRect = new Rect(60f, 200f, 110f, ELEMENT_HEIGHT);
            GUI.color = Color.red;
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (movementItem == movementList.Count - 1)
                {
                    movementList.RemoveAt(movementItem);
                    movementItem--;
                }
                else if (movementList.Count > 1)
                {
                    movementList.RemoveAt(movementItem);
                }
                else
                {
                    Debug.Log("Cannot delete only waypoint");
                }
            }
        }

        GUI.color = Color.white;

        //if(GUI.Button())
    }

    void EffectsGUI()
    {
        offsetX = 510f;
        offsetY = 100f;
        //sets up rects for showing info
        Rect effectsLabel = new Rect(545f, 10f, 250f, ELEMENT_HEIGHT);
        
        offsetY += 20f;
        //Setup to draw a line
        Vector2 linePointTop = new Vector2(750, 0);
        Vector2 linePointBottom = new Vector2(750, 500);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.green, 1f, true);

        EditorGUI.LabelField(effectsLabel, "EFFECTS WAYPOINTS");

        Rect numberLabel = new Rect(600f, 30f, 350f, ELEMENT_HEIGHT);

        EditorGUI.LabelField(numberLabel, ((effectsItem + 1) + " / " + effectsList.Count));
        

        //Waypoint Name
        offsetY -= 70f;
        Rect nameLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);

        EditorGUI.LabelField(nameLabel, "Waypoint Name: ");
        offsetX += 110f;
        Rect nameInputItem = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
        
        effectsList[effectsItem].name = EditorGUI.TextField(nameInputItem, effectsList[effectsItem].name);

        offsetX -= 110f;
        offsetY += 20f;
        Rect windowDisplay = new Rect(offsetX, offsetY, 225f, ELEMENT_HEIGHT);
        offsetX = 0f;
        offsetY = 0f;
        effectsList[effectsItem].type = (EffectTypes)EditorGUI.EnumPopup(windowDisplay, effectsList[effectsItem].type);

        switch(effectsList[effectsItem].type)
        {
            case EffectTypes.SHAKE:
                offsetX += 510f;
                offsetY += 70f;
                //Rect cameraToShakeRect = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                //EditorGUI.LabelField(cameraToShakeRect, "Camera To Shake: ");

                offsetX += 125f;
                //Rect camerToShakeItem = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                //effectsList[effectsItem].shakeCamera = (GameObject)EditorGUI.ObjectField(camerToShakeItem, effectsList[effectsItem].shakeCamera, typeof(GameObject), true);

                offsetY += 20f;
                offsetX -= 125f;
                Rect shakeIntensityLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(shakeIntensityLabel, "Shake Intensity: ");
                
                offsetX += 125f;
                Rect shakeIntensityRect = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                effectsList[effectsItem].intensity = EditorGUI.FloatField(shakeIntensityRect, effectsList[effectsItem].intensity);

                offsetX -= 125f;
                offsetY += 20f;
                Rect shakeDurationLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(shakeDurationLabel, "Shake Duration: ");
                
                offsetX += 125f;
                Rect shakeDurationRect = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX = 0f;
                effectsList[effectsItem].duration = EditorGUI.FloatField(shakeDurationRect, effectsList[effectsItem].duration);        
                break;
            case EffectTypes.SPLATTER:
                offsetX += 510f;
                offsetY += 90f;
                Rect splatterImageLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(splatterImageLabel, "Splatter Image: ");

                offsetX += 100f;
                Rect splatterImageItem = new Rect(offsetX, offsetY, 120f, ELEMENT_HEIGHT);
                effectsList[effectsItem].splatterImage = (Image)EditorGUI.ObjectField(splatterImageItem, effectsList[effectsItem].splatterImage, typeof(Image), true);

                offsetX -= 100f;
                offsetY += 20f;
                Rect splatterFadeInTimeLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(splatterFadeInTimeLabel, "Fade In Time: ");

                offsetX += 100f;
                Rect splatterFadeInTimeItem = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                effectsList[effectsItem].fadeInTime = EditorGUI.FloatField(splatterFadeInTimeItem, effectsList[effectsItem].fadeInTime);

                offsetX -= 100f;
                offsetY += 20f;
                Rect splatterFadeOutTimeLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(splatterFadeOutTimeLabel, "Fade Out Time: ");

                offsetX += 100f;
                Rect splatterFadeOutTimeItem = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                effectsList[effectsItem].fadeOutTime = EditorGUI.FloatField(splatterFadeOutTimeItem, effectsList[effectsItem].fadeOutTime);
                offsetY += 20f;
                offsetX -= 100f;
                Rect splatterWaitTimeLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(splatterWaitTimeLabel, "Wait Time: ");

                offsetX += 100f;
                Rect splatterWaitTimeItem = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX = 0f;
                effectsList[effectsItem].duration = EditorGUI.FloatField(splatterWaitTimeItem, effectsList[effectsItem].duration);
                break;

            case EffectTypes.FADE:
                offsetX += 510f;
                offsetY += 90f;
                Rect fadeImageLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(fadeImageLabel, "Fade image: ");

                offsetX += 100f;
                Rect fadeImageItem = new Rect(offsetX, offsetY, 120f, ELEMENT_HEIGHT);
                effectsList[effectsItem].fadeImage = (Image)EditorGUI.ObjectField(fadeImageItem, effectsList[effectsItem].fadeImage, typeof(Image), true);

                offsetX -= 100f;
                offsetY += 20f;
                Rect fadeInLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(fadeInLabel, "Fade In? ");

                offsetX += 100f;
                Rect fadeInToggle = new Rect(offsetX, offsetY, 10f, ELEMENT_HEIGHT);
                effectsList[effectsItem].fadeInFromBlack = EditorGUI.Toggle(fadeInToggle, effectsList[effectsItem].fadeInFromBlack);

                offsetX -= 100f;
                offsetY += 20f;
                Rect fadeOutLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(fadeOutLabel, "Fade Out? ");

                offsetX += 100f;
                Rect fadeOutToggle = new Rect(offsetX, offsetY, 10f, ELEMENT_HEIGHT);
                effectsList[effectsItem].fadeOutToBlack = EditorGUI.Toggle(fadeOutToggle, effectsList[effectsItem].fadeOutToBlack);

                offsetX -= 100f;
                offsetY += 20f;
                Rect fadeDurationLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(fadeDurationLabel, "Fade Time: ");

                offsetX += 100f;
                Rect fadeDurationItem = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX = 0f;
                effectsList[effectsItem].duration = EditorGUI.FloatField(fadeDurationItem, effectsList[effectsItem].duration);
                break;

            case EffectTypes.WAIT:
                offsetX = 510f;
                offsetY += 90f;
                Rect waitTimeLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 120f;
                
                EditorGUI.LabelField(waitTimeLabel, "Wait Time: ");
                Rect waitRect = new Rect(offsetX, offsetY, 30, ELEMENT_HEIGHT);
                offsetX = 0f;
                effectsList[effectsItem].waitTime = EditorGUI.FloatField(waitRect, movementList[movementItem].waitTime);
                break;
        }

        Rect prevButtonRect = new Rect(560f, 175f, 40f, ELEMENT_HEIGHT);
        if (effectsItem != 0)
        {
            if (GUI.Button(prevButtonRect, "Prev", miniLeft))
            {
                //Debug.Log("Hit previous");
                effectsItem--;
            }
        }

        Rect addButtonRect = new Rect(600f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(addButtonRect, "Add", miniCenter))
        {
            Debug.Log("Hit add");
            effectsList.Insert(effectsItem + 1, new ScriptsEffects());
        }

        Rect nextButtonRect = new Rect(640f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(nextButtonRect, "Next", miniRight))
        {
            Debug.Log("Hit next");
            if (effectsItem < effectsList.Count - 1)
                effectsItem++;
            else
            {
                effectsList.Add(new ScriptsEffects());
                effectsItem++;
            }
        }

        if (effectsList.Count > 1)
        {
            Rect deleteRect = new Rect(580f, 200f, 110f, ELEMENT_HEIGHT);
            GUI.color = Color.red;
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (effectsItem == effectsList.Count - 1)
                {
                    effectsList.RemoveAt(movementItem);
                    effectsItem--;
                }
                else if (effectsList.Count > 1)
                {
                    effectsList.RemoveAt(effectsItem);
                }
                else
                {
                    Debug.Log("Cannot delete only waypoint");
                }
            }
        }

        GUI.color = Color.white;
    }

    void FacingsGUI()
    {
        offsetX = 0;
        offsetY = 0;
        //sets up rects for showing info
        Rect windowDisplay = new Rect(295f, 10f, 250f, ELEMENT_HEIGHT);
        offsetY = 10f;
        //Setup to draw a line
        Vector2 linePointTop = new Vector2(500, 0);
        Vector2 linePointBottom = new Vector2(500, 500);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.green, 1f, true);

        EditorGUI.LabelField(windowDisplay, "FACINGS WAYPOINTS");

        //facing
        Rect numberLabel = new Rect(340f, 30, 200f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(numberLabel, ((facingItem + 1) + " / " + facingList.Count));

        offsetY += ELEMENT_HEIGHT + 2f;
        windowDisplay = new Rect(250f, 50f, 100f, ELEMENT_HEIGHT);
        offsetX += 100;
        EditorGUI.LabelField(windowDisplay, "Facing Type: ");

        windowDisplay = new Rect(350f, 50f, 100f, ELEMENT_HEIGHT);
        offsetX = 260f;
        offsetY += ELEMENT_HEIGHT + 2f;
        facingList[facingItem].name = EditorGUI.TextField(windowDisplay, facingList[facingItem].name);

        offsetX = 250f;
        offsetY += ELEMENT_HEIGHT * 2 -10;
        windowDisplay = new Rect(offsetX, offsetY, 200f, ELEMENT_HEIGHT);
        offsetX = 0f;
        facingList[facingItem].type = (FacingTypes)EditorGUI.EnumPopup(windowDisplay, facingList[facingItem].type);

        offsetY += ELEMENT_HEIGHT;

        switch(facingList[facingItem].type)
        {
            case FacingTypes.LOOKANDRETURN:
                offsetX += 250f;
                windowDisplay = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(windowDisplay, "Look at target: ");

                offsetX += 100f;
                windowDisplay = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                offsetX -= 100f;
                offsetY += 20f;
                facingList[facingItem].lookTarget = (GameObject)EditorGUI.ObjectField(windowDisplay, facingList[facingItem].lookTarget, typeof(GameObject), true);

                windowDisplay = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(windowDisplay, "Look at Target: ");

                offsetX += 100f;
                windowDisplay = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 33f;
                
                facingList[facingItem].speed = EditorGUI.FloatField(windowDisplay, facingList[facingItem].speed);

                windowDisplay = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX = 0f;
                offsetY = 0f;
                EditorGUI.LabelField(windowDisplay, "secs");           
                break;

            case FacingTypes.FREECAMERA:
                offsetX += 250f;
                Rect freelookLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                
                EditorGUI.LabelField(freelookLabel, "Free Camera: ");
                offsetX += 100f;
                Rect canLookToggle = new Rect(offsetX, offsetY, 10f, ELEMENT_HEIGHT);
                facingList[facingItem].canlook = EditorGUI.Toggle(canLookToggle, facingList[facingItem].canlook);
                offsetX = 0f;
                break;

            case FacingTypes.FORCEDLOOK:
                offsetX += 250f;
                Rect forcedLookLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(forcedLookLabel, "Position to Look: ");

                offsetX += 125f;
                Rect forcedLookItem = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                facingList[facingItem].lookTarget = (GameObject)EditorGUI.ObjectField(forcedLookItem, facingList[facingItem].lookTarget, typeof(GameObject), true);

                offsetX -= 125f;
                offsetY += 20f;
                Rect timeToLookLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(timeToLookLabel, "Look Time: ");

                offsetX += 125f;
                Rect timeToLookItem = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                facingList[facingItem].timeToLook = EditorGUI.FloatField(timeToLookItem, facingList[facingItem].timeToLook);
                offsetX = 0f;
                offsetY = 0f;
                break;

            case FacingTypes.WAIT:
                offsetX += 250f;
                Rect waitTimeLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(waitTimeLabel, "Wait Time: ");
                offsetX += 100f;

                Rect waitRect = new Rect(offsetX, offsetY, 30, ELEMENT_HEIGHT);
                facingList[facingItem].waitTime = EditorGUI.FloatField(waitRect, facingList[facingItem].waitTime);
                offsetX = 0f;
                offsetY = 0f;
                break;
                    
        }

        Rect prevButtonRect = new Rect(250f, 175f, 40f, ELEMENT_HEIGHT);
        if (facingItem != 0)
        {
            if (GUI.Button(prevButtonRect, "Prev", miniLeft))
            {
                //Debug.Log("Hit previous");
                facingItem--;
            }
        }
        Rect addButtonRect = new Rect(290f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(addButtonRect, "Add", miniCenter))
        {
            Debug.Log("Hit add");
            facingList.Insert(facingItem + 1, new ScriptFacings());
        }
        Rect nextButtonRect = new Rect(330f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(nextButtonRect, "Next", miniRight))
        {
            Debug.Log("Hit next2");
            if (facingItem < facingList.Count - 1)
                facingItem++;
            else
            {
                facingList.Add(new ScriptFacings());
                facingItem++;
            }
        }

        if (facingList.Count > 1)
        {
            Rect deleteRect = new Rect(260f, 200f, 110f, ELEMENT_HEIGHT);
            GUI.color = Color.red;
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (facingItem == facingList.Count - 1)
                {
                    facingList.RemoveAt(facingItem);
                    facingItem--;
                }
                else if (facingList.Count > 1)
                {
                    facingList.RemoveAt(facingItem);
                }
                else
                {
                    Debug.Log("Cannot delete only waypoint");
                }
            }
        }

        GUI.color = Color.white;
    }

    void OnLostFocus()
    {
        PushData();
    }

    void PushData()
    {
        engineScript.movementList = movementList;
        engineScript.effectsList = effectsList;
        engineScript.facingList = facingList;
        engineScript.lastEditedMovement = movementItem;
        engineScript.lastEditedFacing = facingItem;
    }
}
