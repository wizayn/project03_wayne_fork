﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptFacings
{
    public FacingTypes type;

    public GameObject lookTarget;

    public float speed;
    public float timeToLook;
    public float waitTime;

    public bool canlook;
    public string name;

    
}
