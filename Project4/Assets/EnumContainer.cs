﻿public enum MovementTypes
{
    WAIT,
    STRAIGHT,
    BEIZIER,
    ROTATEANDRETURN  
}

public enum FacingTypes
{
    WAIT,
    FREECAMERA,
    LOOKANDRETURN,
    FORCEDLOOK
    
}

public enum EffectTypes
{
    WAIT,
    SHAKE,
    SPLATTER,
    FADE
    
}