﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class ScriptsEffects
{
    public EffectTypes type;

    public GameObject endPoint;
    public GameObject shakeCamera;

    public Image splatterImage;
    public Image fadeImage;

    public float duration;
    public float intensity;

    public float fadeInTime;
    public float fadeOutTime;
    public float splatterFadeInTime;
    public float splatterFadeOutTime;

    public float waitTime;

    public bool fadeInFromBlack;
    public bool fadeOutToBlack;
    public bool useSplatterFadeIn;
    public bool useSplatterFadeOut;

    
    public string name;
}
